\startproduct supp-mm
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: Scheduling with Linked Lists}]


\startsubject[title={Linked Lists}]

In this exercise you are asked to select the next process to be scheduled from
a list of given processes.
This list may be arbitrarily long and thus an array won't suffice to hold it.
Instead, we introduce a new data structure, called \emph{linked list}.
Spoken more precisely, the structure used is a \emph{circular doubly-linked list}.
%
\startplacefigure[reference=fig:ll,title={Linked List with two elements}]
\hbox{%
\starttikzpicture[auto]
\node[draw] (dummy) {dummy};
\node[draw,right=of dummy] (1) {node-1};
\node[draw,right=of 1] (2) {node-2};

\node[below left=of dummy] (head) {head};

\draw[->] (head) -- (dummy);

\draw[->] (dummy) edge[bend left] (1);
\draw[->] (1)     edge[bend left] (dummy);
\draw[->] (1)     edge[bend left] (2);
\draw[->] (2)     edge[bend left] (1);
\draw[->] (2)     edge[bend right,out=200,in=340,looseness=2.2] (dummy);
\draw[->] (dummy) edge[bend right,out=200,in=340,looseness=2.2] (2);
\stoptikzpicture
}
\stopplacefigure
%
In order to build a linked list of such nodes, we need to define a new type.
This is already done by us, but, just for reference, a possible
definition of a type named \type{struct node} for use in a doubly linked list
is given in \in{Listing}[lst:node].
%
\startplacelisting[reference=lst:node,title={A node in a doubly linked list}]
\startccode
struct node {
	struct node *next;
	struct node *prev;

	int value;
};
\stopccode
\stopplacelisting
%
However, this only defines a new type but doesn't define a new variable
\emph{of} this type.  Again, for reference only, we give a definition of
the dummy node.
%
\startplacelisting[reference=lst:nodedef, title={Defining a linked list node}]
\startccode
struct process dummy = {
	.next = &dummy,
	.prev = &dummy,
	/* we don't care about the dummys value(s) */
};
\stopccode
\stopplacelisting
%
We need the dummy, as otherwise we wouldn't be
able to model an empty list with our data structure (cf.\@
\in{Listing}[fig:emptylist]).
%
\startplacefigure[reference=fig:emptylist,title=An empty list]
\hbox{%
\starttikzpicture[auto]
\node[draw] (dummy) {dummy};
\draw[->] (dummy) edge[loop,out=20,in=160,looseness=5] node {next} (dummy);
\draw[->] (dummy) edge[loop,out=200,in=340,looseness=5] node {prev} (dummy);
\stoptikzpicture
}
\stopplacefigure
%
When we later want to give this list to another function, we don't want to give
it a copy of the dummy value, so we create a pointer to our dummy value
instead and will call it \quote{head} as it \emph{points to the head of the list}.

This will be the variable that is passed to your functions!
\placefloats
\stopsubject


\startsubject[title={Iterating over Linked Lists}]

Luckily, in this exercise you are not asked to manage the linked list (i.e.,
add new elements), but only to iterate over its elements, finding the best
candidate to schedule next.
We provide you with a wrapper which simply calls your function (e.g., \type{spn()})
each time for \emph{every tick} with the \emph{current} list of processes.
It is then the task of your function to select the process that is to run next
by setting its \type{state} to \type{PS_RUNNING} and making sure that all other
processes are \type{PS_READY} or \type{PS_DEAD} (no two processes can be running
at the same time).
You then end your function, the simulation will \quote{run} your process and
your function will be called again.

We start by creating a pointer which will, after we iterated through the whole
list, point to the process that is supposed to run.
However, before iteration we haven't found such a process, thus it's reasonable
to set it to \type{NULL}.
%
\startplacelisting[reference=lst:iterate,title={Iterating over a linked list}]
\startccode
struct process *selected = NULL;
for (struct process *c = head->next;
     	c != head;
     	c = c->next) {
	//TODO: Update `selected'
}
\stopccode
\stopplacelisting
%
We then setup an iteration pointer \type{c} which is initialized to the first
element of our list (remember, \type{head} points to the dummy element).
We want to terminate iteration, when \type{c} points to the same element as
\type{head} points (i.e., the dummy), and after each iteration we update
\type{c} to point to its following node.
%
\startplacefigure[reference=fig:iterate,title={Iterating over a linked list}]
\hbox{%
\starttikzpicture[auto]
\node[draw] (dummy) {dummy};
\node[draw,right=of dummy] (1) {node-1};
\node[draw,right=of 1] (2) {node-2};

\node[below left=of dummy] (head) {head};
\node[below right=of dummy] (c) {c};

\draw[->] (head) -- (dummy);
\draw[->,dotted] (c) edge node {3.} (dummy);
\draw[->] (c) edge node [left] {\textbf{1.}} (1);
\draw[->,dotted] (c) edge node [left] {2.} (2);

\draw[->] (dummy) edge[bend left] (1);
\draw[->] (1)     edge[bend left] (dummy);
\draw[->] (1)     edge[bend left] (2);
\draw[->] (2)     edge[bend left] (1);
\draw[->] (2)     edge[bend right,out=200,in=340,looseness=2.2] (dummy);
\draw[->] (dummy) edge[bend right,out=200,in=340,looseness=2.2] (2);
\stoptikzpicture
}
\stopplacefigure
%
In order to find the process with the shortest runtime, we simply check, wether
we have a currently selected process, and that its runtime (\type{cycles_todo},
\type{cycles_done}) is indeed shorter than the runtime of \type{c}.
If not, we update \type{selected} to be \type{c}.
As we go, we also set the currently running process back to be \type{PS_READY}
or to \type{PS_DEAD} if it has no ticks left to run.

After iteration, we simply set the currently selected process to be running
(this may be the same process as before), and exit our function.

\placefloats
\stopsubject

\stopproduct
