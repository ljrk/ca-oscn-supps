\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: UNIX Signals}]

\startsubject[title={Inter-Process Communication (IPC)}]

One of the most crucial tasks of an OS kernel is to separate running programs
into isolated processes that cannot interfere with each others execution.
Ideally, the processes do not even know whether other processes are running.

However, in reality, we want to have processes communicate and cooperate with
each other---but confined to small, controlled channels of communication.
Inter-Process Communication is a broad categery of techniques our OS provides to
do just that.
For example, a process $A$ may ask the OS to deliver a message to process $B$.
Likewise, the process $B$ might answer with another message.

In this exercise, we look at a specific IPC technology called \emph{signals}.
There is a fixed number of these signals which can be sent from one process
to another to, e.g., signal the request for process termination (\type{SIGTERM})
or interruption (\type{SIGINT}).\footnote{%
  Ever wondered what the \quote{red x} symbol in the upper right corner of the
  window border did? It sends \type{SIGTERM} to the process running in that
  window!
}

The receiving process can catch those signals.
It can comply with the request (often process termination in the above two cases),
ignore them, or do something wholly different.
We will now write a program called \type{evil} which runs endlessly doing
nothing and but will catch both of these signals and chooses to ignore them.

Afterwards, we write a small program called \type{kill}, modeled after UNIX
\goto{\type{kill(1)}}[https://pubs.opengroup.org/onlinepubs/9699919799/utilities/kill.html]
utility, which can send arbitrary signals to a given process.
We can observe, that it's not able to terminate our \quote{evil} process using
the mentioned signals.
\stopsubject


\startsubject[title={An evil plan}]

Catching a signal consists of two steps: Writing a function that is to be
executed when the signal is received, and actually registering this function with our
operating system.

Our function has to have the signature \type{void foo(int)} as this is the
fixed signature for \emph{every} signal handler.
Let's write one:
\startccode
void my_signal_handler(int signal_number)
{
        (void)signal_number;
}
\stopccode
Right now, this function does precisely nothing. The only \quote{statement}
it contains serves the only use to \quote{silence} the compiler complaining
about the variable being unused.

In order to now register this function with our system, we need to call the
\goto{\type{sigaction(2)}}[https://pubs.opengroup.org/onlinepubs/9699919799/functions/sigaction.html]
function.
Unfortunately, in order for Linux to provide us with this function, we need
to type
\startccode
#define _POSIX_C_SOURCE 199309L
\stopccode
at the start of our C file \emph{before} the header includes, as this is a
feature of the POSIX version from the 9th of 1993.

The \type{sigaction} function takes three arguments, first, the signal to be caught, then
a \quote{structure} defining which function is to be called (i.e., \type{my_signal_handler}),
and finally an optional argument where we simply pass \type{NULL}.

Structures are the C way to build custom types by combining other types.
Simply for reference, the structure that is passed to the \type{sigaction}
function looks like:
\startccode
// only for reference:
struct sigaction {
        void (*sa_handler) (int);
        sigset_t sa_mask;
        int sa_flags;
        void (*sa_sigaction)(int, siginfo_t *, void *);
}
\stopccode
It contains foru members of which we will ignore all but the first, the others
will be set to zero or unset.
The first member has type \quote{\emph{pointer to} a function taking \type{int}
and returning \type{void})---which matches our function definition of our
signal catcher above!

Now, let's write our \type{main} function to register the signal handler:
\startccode
#define _POSIX_C_SOURCE 199309L

// ...

int main(void)
{
        struct sigaction sa_register = {
                .sa_handler = &my_signal_handler,
                .sa_flags = 0,
        };
        sigemptyset(&sa_register.sa_mask);

        //TODO
}
\stopccode
We first define a new variable \type{sa_register} of type \type{struct sigaction}
(the \type{struct} keyword is required!) and we immediately assign values to
its members.
As we don't have any special requirements, we can set \type{sa_flags} to $0$
and we \emph{must} leave \type{sa_sigaction} \emph{unset}.\footnote{%
  In this specific notation and case, we could actually omit the line setting
  \type{sa_flags} (but not \type{sa_mask}).
  Furthermore, not setting \type{sa_sigaction} is required since it actually shares
  memory storage with \type{sa_handler}.

  But this is a pecularity of C which you should only use if you are more
  acquainted to the language.
}
However, we must set \type{sa_mask} to empty via a special function, which
takes the address of the member \type{sa_mask} of this variable \type{sa_register}
and sets it to some implementation-defined \quote{empty} value.

We now are prepared to register our function for \type{SIGINT} (and \type{SIGTERM}):
\startccode
int err = sigaction(SIGINT, &sa_register, NULL);
// error handling recommended but ommitted here.
\stopccode

Finally, we simply do an endless loop, thus not terminating the program at
any point.

In the actual exercise you are also asked to print the process ID (PID) at startup,
a timed message in the loop, as well as some text within the signal handler---%
but we leave that for you to figure out.
\stopsubject


\startsubject[title={Killing all evil}]

The \type{kill} program takes an option with a number like \type{-2}, as well
as the PID of the target process.
That is, if our \type{evil} program has PID $1234$, we could run
\startnocode
$ ./kill -2 1234
\stopnocode
To send the signal $2$ or \type{SIGINT} (as documented in
\goto{\type{kill(1)}}[https://pubs.opengroup.org/onlinepubs/9699919799/utilities/kill.html])
to said program, triggering the signal handler.
Alternatively, we can type \type{Ctrl+C} in the running \type{evil} program,
which has the same effect.

So our program asks for two arguments, the first which is in \type{argv[1]}
can be parsed using \type{sscanf}:
\startccode
int n = sscanf(argv[1], "-%d", &sig);
\stopccode
Similarly, our PID in \type{argv[2]} can be parsed, however a PID is not an
\type{int} but a \type{long int}, thus:
\startccode
int m = sscanf(argv[2], "%ld", &pid);
\stopccode

The final step is to call the equally named \emph{function}
\goto{\type{kill(2)}}[https://pubs.opengroup.org/onlinepubs/9699919799/functions/kill.html]):
\startccode
int err = kill(pid, sig);
\stopccode
As with \type{sigaction} we need to request the appropriate POSIX standard before
our headers.

With all this done, are you able to still kill your \quote{evil} program?
Using which signal?
\stopsubject



\stopproduct
