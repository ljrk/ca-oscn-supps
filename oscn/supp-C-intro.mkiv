\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: First C Program}]

\startsubject[title={Program Startup}]

If you've taken the course \quote{Computer Architecture} already, you have
seen us talking much about entry points already, be it a function entry point
called by our wrappers (e.g., \type{gauss}) or the whole programs entry point
(\type{_start}).
We will now use neither, but use the entry point provided by the C standard
library which requires us to in turn to write a function called \type{main}.
But let's recap a bit on entry points---or for those who are new, actually
explain what this concept actually means.

\startplacefigure[reference=fig:crt0,title={libc provides entry point}]
\hbox{
\starttikzpicture[
  box/.style={draw,minimum height=1.5cm,minimum width=1.5cm},
  node distance=2cm,
]
\node[box,label={165:main:}] (main) {};
\node[box,label={165:_start:},left=of main] (libc) {libc};
\node[left=of libc] (os) {OS};

\draw[->] (os) to (libc.west);
\draw[->] (libc.south) to[bend left] (os);

\draw[->] (libc.east) to (main.west);
\draw[->] (main.south) to[bend left] (libc);
\stoptikzpicture
}
\stopplacefigure

In short, an entry point marks the start of our program.
In almost every case, we need to provide one so that when the user executes our
binary it knows where to start executing the instructions.
The C standard defines\footnote{%
  At least for \quote{hosted} environments, which we have/use.
}
the programs entry point to be a function called \type{main} with either of
the signatures given in \in{Listing}[lst:mainsig].
\startplacelisting[title={\type{main} function signatures},reference=lst:mainsig]
\startccode
int main(void);
int main(int argc, char *argv[]);
\stopccode
\stopplacelisting
There are a number of equivalent forms (especially for the latter variant), but
we encourage you not to diverge from the ones given here..
We want to especially highlight the \type{void} within the parenthesis of the
first form, denoting that in this case the function doesn't take any arguments.
In fact, \emph{any function} taking no arguments has to be marked that way,
simply writing \type{foo()} doesn't have the same effect and, by all means,
usually \emph{wrong}.\footnote{%
  This has to do with pre-C89 syntax of function declarations which didn't
  have any prototypes.  You may read up on K&R style declarations for more.
}

\stopsubject


\startsubject[title={Hello World}]

An example code goes farther than a thousand words, so in
\in{Listing}[lst:cmin] we give you the rather famous \quote{Hello World!}
program:
\startplacelisting[title={Minimal C Program},reference=lst:cmin]
\startccode
#include <stdio.h> // <- including a header

int main(void)
{
        puts("Hello World!\n"); // call to puts
        return (0); // not a function call!
}
\stopccode
\stopplacelisting

If we save this code into a text file called, e.g., \type{prog.c}, we can
compile \& link it using (don't type the \type{$}, this simply indicates shell
input here):
\startnocode
$ c99 -Wall -Wextra -pedantic -O2 -c -o prog.o prog.c
$ c99 -o prog prog.o
\stopnocode
The flags \type{-Wxxx}, \type{-pedantic} and \type{-O2} are---strictly speaking---%
not crucially necessary for compilation, but we require your programs to compile
and run with these options and not issuing any warnings when compiled!

To run the program, we can simply issue:
\startnocode
$ ./prog
\stopnocode
It will print out the string \quote{Hello World!} and then exit immediately.

Let's go through the code, step by step.
It defines a function which doesn't take any arguments and returns an integer.
This integer will be the exit-code of our program, on UNIX the convention is to
use \digits{0} for a \quote{successful} exit and non-zero for (different) error
codes.
Although an \type{int} is \unit{4 Byte} big on our platform, in case of the
\type{main} function only the values $[0,128)$ are actually valid exit-codes.
If you see any one writing \type{return (-1)} for \type{main} in a document or
online resource, this is a good time to close that resource and search for
better examples.

The want the \type{main} function to always exit with success.
However, before that, we want to \quote{print} to the console.
In order to do so, we need to access the \type{puts} function from the
standard library.
The documentation, either accessed
\goto{via browser}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/puts.html)]
or by typing \type{man 3 puts} in the console tells us that we need to
\quote{include} the header file \type{stdio.h} at the very start of our
program, in order to correctly use the function.

\stopsubject


\startsubject[title={Program Arguments}]

Often, however, we want to run a program with so-called program arguments.
For example, when compiling
\startccode
$ c99 -Wall -Wextra -pedantic -O2 -c -o prog.o prog.c
\stopccode
we run the program \type{c99} with the arguments \type{-Wall}, \type{-Wextra}
and so on.

Let's do the same with our program.
The operating system we are running our programs under allows us to access
these arguments if we use the second variant of our \type{main}
function, as introduced earlier:
\startccode
#include <stdio.h>

int main(int argc, char *argv[])
{
        for (int i = 1; i < argc; i++) {
                puts(argv[i]);
        }
        return (0);
}
\stopccode
We now use a \type{for}-statement to loop over the elements of the
\emph{argument vector} starting with the element $1$ and ending with
$\text{argc}-1$.
This loop works as follows:
\startitemize[n,joinedup]
\item Before the first iteration, assign $i=1$.
\item Check whether $i < \text{argc}$ (or $i \leq \text{argc}-1$).
\item Execute the loops \quote{body} (printing the argument).
\item Increment $i$ by one.
\item Check again, whether $i<\text{argc}$, \ldots
\stopitemize

But why do we start at $1$---in programming it's usually convention to start
at $0$!
Indeed, the \type{argv} arrays actual \emph{first} element has index $0$
and contains the program execution name, in our case the string \type{./prog}.
Try it!

\stopsubject


\startsubject[title={Function Calls}]

Usually, we do far more than just printing out each argument.
For example, we could assume that each argument were a name of a file and
we are supposed to print the contents of the files to the console!

In that case, it's often best to keep the logic separated into another function.
For our example this is ridiculous, but we do it anyway, to show you how that
is done!

\startccode
#include <stdio.h>

/* takes a NUL-terminated pointer to string */
void super_complex_logic(char *s)
{
        puts(s);
}

int main(int argc, char *argv[])
{
        for (int i = 1; i < argc; i++) {
                super_complex_logic(argv[i]);
        }
        return (0);
}
\stopccode

We defined a new function that takes one argument of type \type{char*} (just
like \type{puts}) and does nothing but pass this argument directly to
\type{puts}.
Since the function returns \type{void} (nothing) we don't need a \type{return}
statement.

For those who've been writing assembly before, this is definitely an improvement
as writing new functions was possible in assembly, but daunting.
C makes this easy!

\stopsubject


\stopproduct
