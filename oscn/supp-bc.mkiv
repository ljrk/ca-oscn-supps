\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: Basic Calculator}]

\startsubject[title={ISO C File Streams}]

In the previous exercise we've dealt with file descriptors (which are just
\type{int}s) and the \type{read}
and \type{write} functions which provide only a thin layer of abstraction over
the UNIX/Linux OS below.
While we will come back to these in later exercises when we really need to deal
with OS specific functionality, for regular reading and writing from and to
\quote{real} files (including \type{stdin} and \type{stdout}) the C standard
library provides another set of functions.
These also work on non-UNIX systems and are called \quote{file streams}.

Let's start to rewrite the \type{main} function from last time using this
higher level interface.
As we can see in \in{Listing}[lst:catmainstreams], the first thing we change
is to replace the \type{0} and \type{1} for the standard input/output file
descriptors with the constants \type{stdin} and \type{stdout}.
In order to be able to use these, we need to include the C standard I/O
header \type{stdio.h}.
Further, we replace \type{open} by \type{fopen} with the option
\type{O_RDONLY} being replaced by the option string \type{"r"}.
This function doesn't return an integer anymore, but a \emph{pointer}, specifically
a pointer to some memory holding a value of type \type{FILE}.
This \type{FILE*} represents our opened file, if we could successfully open it;
however, if the requested file doesn't exist or we don't have the permissions
to open it, \type{fopen} will return a special pointer value, called \type{NULL}.
Such a \type{NULL} pointer doesn't point to anything, in fact, trying to access
the memory \quote{behind} it will likely crash your program.
It is thus crucial to check, whether \type{fopen} succeeded, e.g.:
\startccode
if (src == NULL) {
        perror(argv[i]);
        // ...
}
\stopccode
In the given code, we contracted this \type{NULL} check to \type{!src} which is
equivalent and can be read as \quote{if \type{src} does \emph{not} exist}.

Finally, we replace \type{close} by \type{fclose}.

%
\startplacelisting[title={\type{cat(3)} \type{main} with file streams},
                   reference={lst:catmainstreams}]
\startccode
#include <stdio.h>

int main(int argc, char *argv[])
{
        if (argc == 1) {
                // copy would need to be adapted
                // as well ...
                int err = copy(stdin, stdout);
                return (err);
        }

        int status = 0;
        for (int i = 1; i < argc; i++) {
                FILE *src = fopen(argv[i], "r");
                if (!src) {
                          perror(argv[i]);
                          status = 1;
                          continue;
                }

                int err = copy(src, stdout);
                if (err) {
                          status = 1;
                          fclose(src);
                          break;
                }

                fclose(src);
        }
        return (status);
}
\stopccode
\stopplacelisting
%
We would now need modify the \type{copy} function accordingly, however this
would diverge too much from this document.
I want to end this section with a small note though:  Although the streams
interface is (arguably) more simple, it supports not all use-cases the file
descriptor based I/O does.  That's why the \type{cat} program should indeed be
written as we have done.
For our next use case, streams are completely fine though.
\stopsubject


\startsubject[title={Parsing Strings}]

In this exercise you should implement a \emph{simple} calculator.
Again, we write a specific function for this which will not be called \type{copy}
though (this would be rather unintuitive) and only take on argument, e.g.:
\startccode
void process_file(FILE *input);
\stopccode
In order to not completely give off the solution, we will here solve a slightly
modified exercise to the original.
We start with an accumulator variable that's initialized to the value $a=0$.
The user can now input an operator such as \type{+} or \type{-}, \type{*} or
\type{/} followed by an integer $x$.
Afterwards we calculate $a \text{ op } x$ and store this result in $a$.
The user may then enter another operand and integer.

Assume that the input thus comes in the format: operator, integer, operator
integer, and so on.
Between these there may be arbitrary \quote{white space}, i.e., new lines,
tabulator, space, etc\@.
We terminate if no (valid) input was given.

However, the actual input is simply a string of bytes, e.g.,
\type{"+ 42"}.
We need to \emph{parse} it, turning the
the decimal representation of the integer into an actual number.
Fortunately, the C standard library provides method to do just that.
All we need to do is to provide a so-called \quote{format string} which contains
place holders.
When the input is read, it will be matched against those place holders and a
conversion is done:
\startccode
while (1) {
        int a = 0;
        int x;
        char op;
        int n = fscanf(input, " %c %d", &op, &x);
        // ...
}
\stopccode
This is done using the \type{scanf} family of functions.
The format string in our case uses the place holders \type{%c} for a single
character and \type{%d} for decimal integers. Any space matches any number of
white space inbetween, thus the given format string matches inputs like
(\type{\n} denoting a new line):
\startnocode
+ 42
    +      42
+\n    42
\stopnocode
Following the format string we pass pointers to memory locations (using
\type{&} to take the pointer from the original variable) which allow
the function to store the extracted and converted data, \type{char op} for
\type{%c} and \type{int x} for \type{%d}.

But what if we have invalid input?
The \type{scanf} functions return an integer which denotes the number of
\emph{successful} conversions and assignments.
That is, we expect this number to be two, since we have two conversion
specifiers---if it is less, at least one match has failed.
We thus can continue with doing the actual math as seen in
\in{Listing}[lst:switchcase].
%
\startplacelisting[title={Processing the parsed information},
                   reference=lst:switchcase]
\startccode
if (n != 2) {
        // exit our function
        return;
}
switch (op) {
case '+':
        acc += x;
        break;
case '-':
        acc -= x;
        break;
case '*':
        acc *= x;
        break;
case '/':
        acc /= x;
        break;
default:
        // none of the above matched
        return;
}
printf("acc = %d\n", acc);
\stopccode
\stopplacelisting
%
First we to distinguish the four different valid cases for our operator
and the fifth, invalid, case (e.g., input \type{"= 42"}).
This can be done with a switch-case statement which allows to do simple
equality comparison of one expression (\type{op}) against a list of values.
It's important to remember to end each case with a \type{break} statement, as
otherwise it would \quote{fall through}, i.e., in case of the \type{+}, first
\type{acc += x} would be executed, then \type{acc -= x}, then the multiplication
and so forth.

The final step within the loop is to print the current value of the accumulator,
which is done with a function practically opposite:
It takes a format string with place holders, but instead of matching string
input, it replaces these with the values we provide, thus constructing a
string from, e.g., decimals.
In this case, we ask the function to replace the \type{%d} with the decimal
representation of the number stored in \type{acc}.
Note the new line character \type{\n} at the end which is essential for the output actually being
printed.
\stopsubject

\stopproduct
