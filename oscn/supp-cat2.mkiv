\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\startsubject[title={The Con\type{cat}enate Program}]

In theory, the \type{cat(1)} program takes zero to $n$ arguments which are
interpreted as file names and concatenates their contents, printing the
result to the console:
\startnocode
$ cat foo.txt bar.txt baz.txt
I'm a line from foo.txt
I'm two lines ...
... from bar.txt
And another line from baz.txt
\stopnocode

Our simplified version of cat will not take any program arguments.
In that case, both your and the original program, will read the consoles
interactive text input, and \quote{echo} it back to the user.
To get a feel of what we mean, try to replicate the given session.
\startplacelisting[reference=lst:icat,title={Interactive Input/Output with \type{cat(1)}}]
\startnocode[lines=yes]
$ cat
I write a line and press enter
I write a line and press enter
The first line was my input, the second by cat(1)
The first line was my input, the second by cat(1)
I can also write a line and press Ctrl-DI can also write a line and press Ctrl-DThis way, no line break is issued
This way, no line break is issued
To terminate input, we need to end the 'input file'
To terminate input, we need to end the 'input file'
We do that by pressing Ctrl-D twice
\stopnocode
\stopplacelisting

UNIX/Linux programs see interactive console input as one \quote{file}.
What the \type{cat(1)} program does is simply read a chunk of that file, write that
to the console, read another chunk, and so on.

If you type input in the console it will not be \emph{written} to that \quote{file}
immediately though, but only after you \quote{flush the buffer}.
This is done by sending the special ASCII symbol \type{EOT} (End-Of-Transmission)
which can be entered by typing \type{Ctrl-D}.
Alternatively, you can press the \type{ENTER} key which will first append a
newline (\type{'\n'}) at the end of the input and then send \type{EOT}.
The effect can be observed in the \in{Listing}[lst:icat].

In order for \type{cat(1)} to terminate, we need to end the file.
A file has ended, when there's \quote{no more data to read}.
We can signal this condition, also called \type{EOF} (End-Of-File) by flushing
the buffer when there's been no input.
This may be directly after program startup, but also, after just having flushed
the buffer.
Thus, sending \type{EOT} (\type{Ctrl-D}) twice does just that.

I heavily recommend playing around with the original \type{cat(1)} program a
bit as this behavior is better seen than told.
\stopsubject


\startsubject[title={UNIX File I/O---in C}]

Not only is the interactive console input a file, so is the console output.
These files are called \quote{standard input} and \quote{standard output}
respectively.

While a file on disk is identified by its name, in order to read or write from
or to it, a file needs to be opened by the program.
After that, the program can refer to each file by an identifying number,
called file descriptor (type{fd}).
Luckily for us, both standard in- and output are open on program startup with
their fds being being \digits{0} and \digits{1} respectively.

Let's write a function that copies from a given \type{fd} to another.
\startccode
int copy(int srcfd, int destfd)
{
        //TODO
        return (0);
}

int main(void)
{
        int err = copy(0, 1); // copy stdin to stdout
        return (err);
}
\stopccode

Since the input file may be \emph{really} big we cannot copy it in one
session but copy it \unit{1024 Byte} at a time.
For that we need to have a temporary storage location of that size.
We will use an array of \digits{1024} elements of \type{char} since a
\type{char} is exactly \unit{1 Byte}.
\startccode
int copy(int srcfd, int destfd)
{
        char buffer[1024];

        // TODO
        return (0);
}
\stopccode
We call this variable \quote{buffer} (a different one from the mentioned
above with \type{Ctrl-D}, they are really everywhere!) and
can now access any byte in it from the first \type{buffer[0]} to
the last \type{buffer[1023]}.

In order to actually \emph{read} the input we need to execute a system call.
Again, those who've taken Computer Architecture before will remember that these
don't work the same way as regular function \type{call}s.
However, the UNIX standard library header \type{unistd.h} provides us with
friendly wrappers which make them feel like regular functions.
\startccode
#include <unistd.h>

int copy(int srcfd, int destfd)
{
        char buffer[1024];

        ssize_t nread = read(srcfd, buffer, 1024);
        // TODO
        return (0);
}
\stopccode
This will read \emph{up to} \unit{1024 byte} from the input fd into the buffer.
It will \emph{also} return the number of bytes \emph{actually read},
which is assigned to the variable \type{nread}.
This result may be less than \unit{1024 Byte}, if there aren't that many bytes
available.
The special case of \digits{0},
as already mentioned earlier,
indicates EOF and our \type{copy} function should end itself in that case:
\startccode
ssize_t nread = /* ... */
if (nread == 0) {
        return (0);
}
\stopccode

Without going into too much detail, the type \type{ssize_t} is determined by
the result type of \type{read} whicyh you can look up from the documentation at
\goto{\type{read(3p)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/read.html)],
or on the console:
\startnocode
$ man 2 read    # or, if posix-man installed:
$ man 3p read
\stopnocode

The next step is to write those \type{nread} many bytes to \type{destfd} which
works similarly, with the documentation being at
\goto{\type{write(3p)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/write.html)]
\startccode
int copy(int srcfd, int destfd)
{
        char buffer[1024];

        ssize_t nread = read(srcfd, buffer, 1024);
        if (nread == 0) { /* EOF */
            return (0);
        }
        write(destfd, buffer, nread);

        return (0);
}
\stopccode
This will execute once only though. Try it!

In order to make history repeat itself, we wrap the everything from the
\type{read} to the \type{write} into an endless loop:
\startccode
while (1) {
        /* ... */
}
\stopccode
It's not really endless though, since, if the \type{read} function returns $0$
we break out of the loop and terminate the function anyway!
This seems to work now!

Unfortunately, we are not completely done as the \type{write} function has a
small caveat:
Even if we ask it to write \type{nread} many bytes---it may write less
(although, at least \unit{1 Byte}).
We might end up with reading \unit{500 Byte} and then writing just
\unit{200 Byte}!

It does return how many bytes it \emph{did actually write}.
With this information, we can ask it to write out the remaining \unit{300 Byte}.
We do that by keeping track of how many bytes we've written in total.
This is needed as the \type{write} function doesn't remember were it left off
and we need to tell it not to write out those \unit{200 Byte} again, but to
skip those.
In pseudocode this would look like:
\startnocode
nread = read(...)
if nread == 0:
    return (0)

nwritten = 0
while nwritten < nread:
    nwritten' = write(destfd, buffer+nwritten,
                      nread-nwritten)
    nwritten = nwritten + nwritten'
\stopnocode
\stopsubject


\startsubject[title={Program Arguments}]

We've now reached feature parity with the assembly version of our \type{cat(1)}
program.
However, the actual \type{cat(1)} program doesn't read just from standard
input but also allows for program arguments supplying file names of files
which are subsequently printed to the console output.

The following provides a skeleton for what we want to do in our \type{main}
function:
\startccode
int main(int argc, char *argv[])
{
        if (argc == 1) { /* no actual arguments */
                int err = copy(0, 1);
                return (err);
        }

        int status = 0;
        for (int i = 1; i < argc; i++) {
                //TODO:  "create" srcfd
                
                int err = copy(srcfd, 1);
                if (err) {
                        status = 1;
                        // break out of the loop
                        break;
                }
        }
        return (status);
}
\stopccode

What's left is assigning each file name a file descriptor as discussed earlier.
This process is called \quote{opening} a file, which is done using the
\goto{\type{open(3p)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/open.html)]
function:
\startccode
// open the file with name `argv[i]' with the 
// read-only option:
int srcfd = open(argv[i], O_RDONLY);
\stopccode
This function assigns an ID (the \type{fd}) to refer to that file which we now
can use for our \type{copy} function.
After using it, we should however
\goto{\type{close(3p)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/close.html)]
the file descriptor again, in order to not waste IDs.
\stopsubject


\startsubject[title={Error Handling}]
There are many open questions in our program now, however.
Things like, what happens if we pass file names which don't refer to valid
files or files at all?
Or to files we don't have the permission to read?
In that case, the \type{open} function will return \digits{-1} as specified
by the \quote{RETURN VALUE} section in its manual.
Instead of passing it to our \type{copy} function, we should check for this
special value and print an error message:
\startccode
#include <stdio.h>

// ...

if (srcfd == -1) {
        // print an error, e.g.:
        // /etc/shadow: Permission denied
        perror(argv[i]);

        // Denote unsuccessful run, but don't break out
        // yet, just continue with the next file and
        // skip the current.
        status = 1;
        continue;
}
\stopccode
The
\goto{\type{perror(3p)}}[url(https://pubs.opengroup.org/onlinepubs/9699919799/functions/perror.html)]
function is quite handy here, as while the return value of \digits{-1} only
denotes that the function failed, \type{open} has also set the variable
\type{errno} to a special value denoting \emph{how} it has failed.
The \type{perror} function interprets this value and prints the respective
error on the console, prefixed with whatever string we passed it (in our case,
the file name), followed by a colon \type{:} and space.

If \type{open} fails, we simply want to carry on with the next file after printing
the error, while \quote{remembering} that something went wrong, s.t., we can
actually set the exit code (return value of \type{main}) appropriately.
We do that by assigning \digits{1} to a variable we've created and returning
this variable from main, once we've finished copying all other files to the
standard output.

Also note that the \type{read} and \type{write} functions can fail as well (although this
is less likely).
But it's better to check for these errors since otherwise debugging a program
is really difficult.
\stopsubject



\stopproduct
