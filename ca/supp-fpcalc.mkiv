\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: Software FPU}]

\startsubject[title={Short Primer on FPUs}]

For a long time CPUs didn't include support to do more than integer arithmetics.
Instead, a FPU would have to be bought as a co-processor to which the task
of floating point arithmetics was delegated.  E.g., for the 8086 (the ancestor
of the x86 ISA), you could by the 8087 to add FPU support.  Nowadays, FPU
support is built-in and even replaced by so-called SIMD units which are not
only able to do floating point arithmetics but vector processing as well (we
will look into them in more detail later).

However, partly stemming from this, the registers used for floating point are
different than our regular \quote{integer registers}.  In our case, these are
the XMM-registers, being named \type{xmm0} through \type{xmm8}.
The instructions used to act on these are different as well.

In this exercise, your task though is \emph{not} to use the builtin FPU/SIMD
instructions to work with the floating point operands, but rather emulate the
same features via reimplementing the addition algorithm
with integer registers.
\stopsubject


\startsubject[title={Moving from/to SIMD registers}]

Our floating point values reside as \unit{32 bit} values in the \type{xmm0} and
\type{xmm1} registers for the first and second operand respectively.
In order to be able to work on them using our well-known integer instructions
such as \type{add}, \type{sub}, \type{mul}, etc., we need to first move these
values from those XMM-registers to \unit{32 bit} wide regular integer registers.
The instruction to use for that is called \type{movd} which means \quote{move
doubleword}, where a \emph{word} is x86 terminology for \unit{2 byte}, thus
\emph{doubleword} being \unit{4 byte} or \unit{32 bit}.
\startasmcode 
; move floating point operands to integer registers
movd eax, xmm0
movd ecx, xmm1
\stopasmcode

The next step would be to do the actual addition.  Assuming however, we did that
and the correct result were in the \type{eax} register, we would then move it
back to \type{xmm0} as the return value of our function, again using \type{movd}.

Before implementing the actual algorithm, you can test your program already and
check whether it returns the expected value (the value of the first argument
passed).
\stopsubject


\startsubject[title={Bitfiddling}]

Now that our \type{eax} register contains the binary \unit{32 bit} IEEE-754
representation of our number, we need to somehow separate the sign bit, the
exponent (in biased or excess representation) and the significand (or mantissa/%
\hskip0pt coefficient).
%
\startplacefigure[title={Schematic content of our \unit{32 bit} register}]
\hbox{%
\starttikzpicture[
    node distance=0cm,
    minimum height=2em]

  \node[draw] (s) {s};
  \node[draw,right=of s] (e) {\unit{8 bit} biased exp};
  \node[draw,right=of e,minimum width=17em] (m) {\unit{23 bit} mantissa};
\stoptikzpicture
}
\stopplacefigure
%
In order to access single bits or groups of bits within a register, we need to
resort to bitwise operations.
The bitwise and operation goes through each two bits in the operands and does
a logic and of them, i.e., $0 \land 1 = 0$, $1 \land 1 = 1$ and so on.
This can be used to isolate parts of our register, say the biased exponent:
\addfeature[f:tablenum]%
\startformula \startalign
  \NC    \NC 0 \; 10101010 \; 10010000100001000010000 \NR
  \NC \& \NC 0 \; 11111111 \; 00000000000000000000000 \NR
  \NC  = \NC 0 \; 10101010 \; 00000000000000000000000 \NR
\stopalign \stopformula
\resetfeature
In this case we'd also need to move, \quote{shift}, the biased exponent to the
right by the number of digits contained in the mantissa (\digits{23}).

Similarly, after we've done the actual calculation we end up with three registers
containing the sign bit, biased exponent and mantissa separately.
Again, we use bitwise operations to combine them into one, this time using
bitwise or:
\addfeature[f:tablenum]%
\startformula \startalign
  \NC        \NC 0 \; 10101010 \; 00000000000000000000000 \NR
  \NC \mid\; \NC 0 \; 00000000 \; 10010000100001000010000  \NR
  \NC      = \NC 0 \; 10101010 \; 10010000100001000010000 \NR
\stopalign \stopformula
\resetfeature

The relevant assembly instructions for bitwise and, or and shift right as well
as shift left are simply \type{and}, \type{or}, \type{shr} and \type{shl}.
\stopsubject

\stopproduct
