\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: The FPU and the 7\high{th}/8\high{th} args}]

\startsubject[title={Calling Convention:  We need more arguments}]

The System V AMD64 calling convention (the one we use) specifies that the first
\digits{6} \emph{integer} arguments are in the registers
\type{RDI}, \type{RSI}, \type{RDX}, \type{RCX}, \type{R8}, and \type{R9}
respectively (or \type{EDI}, \type{ESI}, \ldots for \unit{32 bit} values).
However, in rare cases, our functions take more arguments, and while the CPU
designers at intel and AMD could have used more registers, the number of
registers is limited.
What, however, is almost unlimited (at least, from our perspective) is memory,
specifically the stack.
Hence, all further arguments are pushed on the stack by the caller before
making the call.

E.g., if the caller wants to pass the value $42$ as the 7\high{th} argument,
and $1234$ as the 8\high{th}, then they could write:
\startasmcode
mov rdi, ...	; 1st arg
; ...
mov r9, ...		; 6th arg
push 1234
push 42
call formula_int
\stopasmcode
Note that the \emph{first} argument is pushed last, making it the
topmost on the stack!

Since the \type{call} instruction also pushes the program counter \type{rip},
which holds the return address to which the program should jump when returning,
on the stack, this return address will be the topmost value on the stack
when our function \type{formula_int} starts.

When we visualize the stack it looks as given in \in{Figure}[fig:stack]
(growing \quote{upwards} to the \emph{lower} addresses):
\startplacefigure[reference=fig:stack,title={Arguments on stack}]
\hbox{%
\pgfmathsetmacro{\myinnersep}{2}
\starttikzpicture[auto,
  stackel/.style={
    draw,
    node distance=0cm,
    text width=5cm,
    minimum height=1em+2*\myinnersep*1mm,
  }
]
\node[stackel] (ret) {return address};
\node[stackel,below=of ret] (7) {7\high{th} arg};
\node[stackel,below=of 7] (8)  {8\high{th} arg};
\node[stackel,dashed,below=of 8] (x)  {\dots};
%\node[stackel,dashed,below=of x,align=center] (y)  {{\it highest address}};
\node[stackel,dotted,above=of ret] (z) {};

\node at ($(ret.north west) + (-3em,0)$) {\type{rsp}};
\draw[<-] ($(ret.north west)$) -- ($(ret.north west) + (-2em,0)$);

\path[->] ($(8.south east) + (2em,0)$) edge node[midway,sloped,below] {growth direction} ($(ret.north east) + (2em,0)$);
\stoptikzpicture
}
\stopplacefigure

Despite our arguments being only \unit{32 bit} wide, the elements on the stack
are all \unit{64 bit} as we are programming in \unit{64 bit} assembly.
We now need to access these elements while preserving the stack.
One could write the following code to load the 7\high{th} and 8\high{th} arguments
into the registers \type{r10} and \type{r11} respectively:
\startasmcode
pop rax		; store the stack pointer in rax
pop r10		; store 7th arg in r10
pop	r11		; store 8th arg in r11
push r11	; rebuild the stack
push r10
push rax
\stopasmcode
However we can also load the values directly without modifying the stack using
indirect addressing.
Since we know that the return address takes \unit{8 Byte} starting at \type{rsp},
the 7\high{th} argument is at address ${\text{rsp}+8}$ and the 8\high{th} at
${\text{rsp}+16}$:
\startasmcode
mov r10, [rsp+8]
mov r11, [rsp+16]
\stopasmcode
That's it!

One last remark before being done with the integer version of the formula:
Since in this exercise we use \type{int32_t} and not \type{uint32_t}, the
arguments are stored in two's complement, as they may be negative.
This implies that you can't use the \type{MUL} and \type{DIV} instructions but
must use \type{IMUL} and \type{IDIV} instead!
\stopsubject


\startsubject[title={The hardware FPU}]

The floating point unit is the part of the processor that deals with floating
point numbers.
Historically, for the x86, this was the x87 co-processor which is now integrated
into the CPU\@.
However, nowadays we don't use those, rather old, instructions but use the
SIMD/SSE2 unit found in all modern x86 processors.
This unit has its own set of registers, named \type{xmm0} through \type{xmm15}.
Conveniently, all program arguments are simply passed in this order, with
\type{xmm0} also being the return value.

We cannot use the \type{add} instruction to add two of those registers however,
as this instruction only works on integer registers.
Instead, this unit provides us the instructions \type{addss}, \type{addsd},
\type{addps} and \type{addpd}.
The first S or P stands for scalar vs.\@ packed, the second S or D for
single (\unit{32 bit}) vs.\@ double (\unit{64 bit}).
This is because the add instruction can add single-precision floating points
as well as double precision floating point numbers.
Moreover, you can in fact \quote{pack} four single-precision floating point
numbers in one \unit{128 bit} wide \type{xmm} register and add them
\emph{simultaneously} to another set of four single-precision floats.
Alternatively, we can add two packs of two double-precision floating point numbers.

In our case, we don't want this \quote{packed} addition, but scalar
double-precision addition.
Otherwise, all instructions behave as one could intuitively guess, they all
take two operands.

Sadly, however, the following won't work:
\type{divsd xmm6, 2.0}
Neither does \type{movsd xmm8, 2.0} work.
In order to use constants like $2.0$, $3.0$, etc., we need to provide them as
constants in memory.
To do that, we need to open another so-called \quote{section} in our code which
contains those constants:
\startasmcode
SECTION .data	; all the following are constants

two:	dq 2.0	; store a quadword (64 bit) 

SECTION .text	; all the following is code again

formula_flt:	; ...
\stopasmcode
In order to access these constants we simply do:
\startasmcode
divsd xmm6, [rel two]
\stopasmcode
The \type{rel} specifier is an implementation-detail you don't need to
understand.
It's necessary since due to a multitude of reasons we need to instruct NASM
to do \emph{PC-relative} addressing instead of absolute addressing.

\stopsubject

\stopproduct
