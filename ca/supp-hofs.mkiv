\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: Function Pointers}]

\startsubject[title={Higher Order Functions}]

Till now, all operands to the functions you are supposed to write are simple
data.
But, what if, we could pass another function to our function?
In Haskell, this concept can look like the following:\footnote{%
  In this case, a true Haskellian would liekly not pass the comparison function
  at all but simply rely on \type{Ord} but that defeate the purpose of this
  example.
}
\starthaskellcode
sort :: (a -> a -> Ordering) -> [a] -> [a]
\stophaskellcode
For those who know Haskell, this signature tells us that our function takes
two arguments.
First, a function that in turn gets two arguments of the same type.
This function performs \emph{compares} its two arguments and returns an
\type{Ordering} (equal, or less or greater than).
The second argument to the \type{sort} function is a list of values of the
same type our comparison function takes.

This \type{sort} function now goes through the list, taking two of its elements
and comparing it with the passed function.  An example insertion sort
algorithm could look like this:
\starthaskellcode
sort :: (a -> a -> Ordering) -> [a] -> [a]
sort cmp []     = []
sort cmp (x:xs) = insert x (sort cmp xs)
  where insert x [] = [x]
        insert x (y:ys)
          | cmp x y == GT = y:(insert x ys)
          | otherwise     = x:y:ys
\stophaskellcode

This technique allows us to have a sort function which is oblivious to the
type of values its actually sorting -- as long as there exists a function
which can give us an ordering for any given two values.

We will now look at how we can re-create this functionality in assembly
using function pointers.
\stopsubject


\startsubject[title={Addresses of Functions}]

A function pointer is just the address of the first instruction in that function.
We've dealt with such addresses in form of their named counter-parts, labels,
already.
Effectively, when having code like this:
\startasmcode
foo:		mov rax, 42
			cmp rdi, 0
			je .bar
.baz:		add rax, rdi
			; ...
.bar:		sub rax, rdi
			; ...
\stopasmcode
The assembler translates all instruction mnemonics into machine instructions
(binary) and places them consecutively into the executable file.
Thus, in memory, \type{mov rax, 42} may take up \unit{9 Byte} and be at address
\digits{1000}.
Then the following instruction \type{cmp rdi, 0} is at address
\unit{1009}.
The assembler furthermore gets rid of the labels, replacing the jump target in
\type{je .bar} by the actual numeric address of the \type{sub rax, rdi}
instruction, say \type{je 1021}.

In fact, as those addresses are simply numbers, we can just do things like these
(please do \emph{do not} do this though):
\startasmcode
foo:		mov rax, .bar
			; `sub rax, rdi' is 3 Bytes wide
			add rax, 3
			je rax			; skip 1st instr in .bar
.baz:		add rax, rdi
			; ...
.bar:		sub rax, rdi
			; ...
\stopasmcode

This means it's possible to write a function which, as its argument,
accepts the starting address of another function, and simply call it:
\startasmcode
foo:	mov rax, rdi	; address of function
		; ...
		mov rdi, 42		; 1st arg for passed func
		call rax
		; ...
\stopasmcode
That's it!  However, we of course need to take great care regarding volatile
and non-volatile registers here as well and not use registers such as
\type{rbx} without first backing them up.
\stopsubject


\startsubject[title={Putting it Together}]

Our \type{sort} function takes the (start address of the) array as its first
argument, with the second argument being the number of elements in the array
(the number of elements isn't needed in the Haskell versions as Haskell encodes
this information within the list).
The third and last argument is the address of the comparison function.

The elements within the array are all \unit{64 bit} wide.\footnote{%
  It would be possible to not have this restriction and allow for e.g.,
  \unit{8 bit} wide elements as well, but this would be unnecessary extra coding
  on your part.
}
However, unlike in Haskell, our comparison function does not take two elements
of our array but two \emph{addresses} of two elements of our array.
That is, if our array looks like:
\startnocode
0xf00: [6, 4, 0, -1]
\stopnocode
We would not call \type{compar} on $6$ and $4$ but on their respective addresses.
If our array starts at address $0\mathrm{x}f00$ this means that its first element
is in the memory on this address $0\mathrm{x}f00$.
As all elements are \unit{64 bit} or \unit{8 Byte} wide, the second element
is at $0\mathrm{x}f00 + 8$, i.e., $0\mathrm{x}f08$.
The third then would be $0\mathrm{x}f10$, then $0\mathrm{x}f18$, and so on and
so forth.
Thus, when we call \type{compar} we pass the values $0\mathrm{x}f00$ and
$0\mathrm{x}f08$.
In assembly, this would look roughly as given in \in{Listing}[lst:lea].%
\footnote
{%
  Note that we used non-volatile registers there, thus we don't need to backup
  and restore them each time we call the \type{compar} function.
  This, however, means we need to push them onto the stack at the very
  beginning of our function and restore them at the end.
}

\startplacelisting[reference=lst:lea,title={Calling \type{compar}}]
\startasmcode
		; r12 is `base', the array address
		; r13 is our current index, e.g., 0
		; r14 is the `compar' function

		; load effective address of current ...
		lea rdi, [r12 + r13*8]
		; ... and next array value
		lea rsi, [r12 + r13*8 +8]
		call r14
\stopasmcode
\stopplacelisting

We can use the \type{lea} instruction to calculate the address of an array
entry by using the base address and adding the number of Bytes as offset.
Again, using the example of $\mathrm{base} = 0\mathrm{x}f00$ (\type{r12})
and index ${i = 0}$ (\type{r13}) we calculate the address of the first
element as $\mathrm{base} + 8i$ since each element consists of
\unit{8 Byte}.
The consecutively following element is another \unit{8 Byte} next.

After having called the \type{compar} function, we need to check its return
value, which can be $-1$ (less than), $0$ (equal), or $1$ (greater than).
In this example, \type{compar(0xf00, 0xf08)} would return $1$ since $6>4$,
so $6$ and $4$ should switch positions.
We cannot, however just exchange the values of \type{rdi} and \type{rsi}!
This is for two reasons:
First, \type{rdi} and \type{rsi} may not contain the values they had anymore,
as these are volatile registers and \type{compar} might have overwritten the
values.
Second, these registers only contained the \emph{addresses} of the array
elements, if we just switch these, we don't modify anything in memory!

Instead, we now need to load the actual values from memory, and switch these,
e.g.:
\startasmcode
		; r12 is `base`, the array address
		; r13 is our current index, e.g., 0
		mov rdi, [r12 + r13*8]
		mov rsi, [r12 + r13*8 +8]
		mov [r12 + r13*8], rsi
		mov [r12 + r13*8 +8], rdi
\stopasmcode

Your task is to iterate over this array and sort the whole array using this
technique combined with a sorting algorithm of your choosing (bubble sort is
rather easy to implement).
\stopsubject


\startsubject[title={Optional: A Note about Stack Alignment}]

In \emph{theory}, there's actually just one more thing to take care of, which is
\quote{Stack Alignment}.
The System V ABI Calling Convention actually \emph{demands}
that if you call a function, the stack is \quote{\unit{16 Byte} aligned}
on entry.
This means that the value of the \type{rsp} is a multiple of \digits{16} when
the first instruction is executed.
While this sounds complicated at first, it's not as bad as it seems:
\startasmcode
foo:				; function entry: we are aligned
		push rbx	; 8 B misaligned (8 B registers!)
		push rcx	; aligned again
		push rdx	; 8 B misaligned
		call bar	; correct function call
		; ...
\stopasmcode
Basically all we'd have to do is make sure that when pushing registers to the
stack, that \emph{before} calling the function, the stack must be
\emph{misaligned} by \unit{8 Byte} or \unit{64 bit}.
This is since the call itself pushes the current instruction pointer to the
stack, thus finally aligning it.

But why was this not yet necessary in the previous exercises?
The functions that you called were all
functions that you programmed yourself---and while the calling convention still
actually demands that the entry point is aligned, your functions didn't require
it as you, yourself, never made use of that \quote{feature}.

This time you call functions from our wrapper that are foreign to your
code, so you'd need to adhere to the calling convention more strictly.  However,
we programmed our function in a way that
\emph{you aren't forced to implement that}.
We just think that it's a thing worth noting---especially if you think about
calling other external functions from the C library instead.

\stopsubject

\stopproduct
