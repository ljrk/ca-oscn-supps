\startproduct *
\project ../proj

\setuphead[title][style={\ss\bfb},
  before={\begingroup},
  after={Leonard König, \currentdate\medskip\endgroup}]

\starttext

\starttitle[title={Supplement: First Assembly Program}]

\startsubject[title={Program Startup}]

You can think of writing a program as writing a manual for a (very) dumb
machine.
This manual consists of instructions which have to be executed in a specific
order to reach the intended goal, but most critically, this instruction
\quote{manual} must have first instruction, the so-called \emph{entry point}.

In many languages, this actual programs entry point is rather hidden.
If you have experience in writing Haskell programs and loading them through
the GHCI, for example, then you will never have written that entry point yourself,
but GHCI is providing it for you with methods to \quote{insert} your code or
function into the already-running program.
That way, your functions only need to implement the \quote{actual} algorithm
and not worry about somehow dealing with user input or printing the result to
the console---GHCI will do all that for you.
You still do provide an entry point: The entry point of your function (in this
exercise called \type{gauss}). Conversely, the Operating System provides an
entry point for the BIOS of the computer to boot it: In the end our system
is simply a huge chain of such \quote{entry points}.

The reason we do that is that this way you don't need to worry about input-output
(I/O) and focus on the actual task at hand.
However we do not have such a powerful tool as GHCI, instead we provide
\quote{wrappers}.
While these are written in C, a compiler can compile these into machine code.
This is then combined with your code to provide a fully functioning program---%
both, your code and our wrapper aren't of any use alone.

\startplacefigure[reference=fig:call-chain,title={Wrapper provides entry
point and chain-calls your code}]
\hbox{
\starttikzpicture[
  box/.style={draw,minimum height=1.5cm,minimum width=1.5cm},
  node distance=2cm,
]
\node[box,label={165:gauss:}] (gauss) {};
\node[box,label={165:_start:},left=of gauss] (wrapper) {wrapper};
\node[left=of wrapper] (os) {OS};

\draw[->] (os) to (wrapper.west);
\draw[->] (wrapper.south) to[bend left] (os);

\draw[->] (wrapper.east) to (gauss.west);
\draw[->] (gauss.south) to[bend left] (wrapper);
\stoptikzpicture
}
\stopplacefigure
\stopsubject

\startsubject[title={Compilation \& Linking}]

Our computer only accepts our \quote{manual} if the instructions given are in
its native language, the machine language.
Different CPUs may have different machine languages, defined by their
Instruction Set Architecture (ISA).
You can consider the ISA as dictionary of words
(instructions) understood by the CPU, documented with their meaning.

The instructions are fed to the CPU from the memory in the format of bytes.
You won't need to learn this \emph{machine language};
but to give a visualisation how this looks, here
are a three instructions for the x86 ISA, in their binary representation:
\startnocode
0101 0101
0101 0011
1100 0011
\stopnocode
Not really spectacular, but not really helpful to us either.

To ease writing programs, programmers have devised other representations of
those instructions that are easier for humans to understand.
These are called mnemonics and they make up an Assembly language.
As the original instructions are ISA specific, their corresponding Assembly
instructions must as well, since they are 1:1 translations.
Moreover, there may be different Assembly \quote{dialects} translating to the
same machine code.
The above could be written in NASM (which is what we will use in this
course) like this:
\startasmcode
push rbp
push rbx
ret
\stopasmcode
Or in a different dialect, GAS, like this:
\startasmcode
push %rbp
push %rbx
retq
\stopasmcode

We can now start writing the \quote{Gauss} program from the exercise.
And while our programs entry point is provided by the wrapper,
the wrapper itself requires your code to start at \quote{label} with the name
\type{gauss} in order to work.
Such a label is set in Assembly by simply writing its name, followed by a
colon, before the first instruction that should be considered part of it.
\footnote{%
  The label only provides start, there's no syntactical way to denote the
  end.
  Instead we rely on assembly instructions to explicitly go back (\quote{return}).
}

Our first implementation of the program won't be \quote{correct} by any means,
we simply \quote{return} immediately, that is, our code \quote{does nothing}.
\startasmcode
; The following line is needed in order for our C
; code to be able to see the gauss label "globally".
global gauss

; label which works as "start-of-function" marker
; and just one instruction to immediately "return"
; from the function call.
gauss:  ret
\stopasmcode

Unfortunately, if the computer can't understand Assembly, we need a translator to
convert the Assembly code into machine code: The Assembler.
We write our code into a plain text file~\footnote{%
  The extension is not relevant, but you may use \type{.asm} or \type{.nasm};
  likewise, the file name is irrelevant.
}, and invoke a command line tool on it (the \type{$} is not typed into the console, it only signifies
that the following is console input):
\startnocode
$ nasm -f elf64 -o my_code.o my_code.asm
\stopnocode
This instructs the NASM assembler to produce output for
our operating system (x64 Linux: ELF64), write the output into a file called
\type{my_code.o}, and with the input code being \type{my_code.asm}.
Files containing machine code are often called object files, hence we chose
the \type{.o} file extension.

As we already established, this isn't a complete program yet---it
still misses the wrapper.
To be able to glue both parts together, we must translate it into machine
code as well.
The wrapper isn't written in Assembly but C, the translator---compiler---must
be a different one as well:
\startnocode
$ c99 -O2 -c -o gauss_wrapper.o gauss_wrapper.c
\stopnocode
The option \type{-O2} tells the compiler to optimize the result for speed
(we require you to use this flag for different reasons!).
The following option switches the compiler into \quote{compile} mode as the
usual C compiler can actually do more than that.
The option with its argument \type{-o gauss_wrapper.o} works the same way as
for NASM\@.

Now we've produced the files \type{gauss.o} and \type{gauss_wrapper.o} from
their respective source files.
In order to create an executable in a process called \quote{linking}, we
again use the C compiler---without the \type{-c} option however!
\startnocode
$ c99 -o gauss gauss_wrapper.o gauss.o
\stopnocode
Without the \quote{compile-only option} the compiler links both files together into an
output we chose to call \type{gauss}.  The name, again, is irrelevant, while
on Windows systems \type{.exe} is used as extension, the extension doesn't
matter on Linux and UNIX\@.

\startplacefigure
\hbox{%
\starttikzpicture[node distance=1.5cm]

\node[draw] (w) {\type{gauss_wrapper.c}};
\node[draw,below=of w] (g) {\type{gauss.asm}};

\node[draw,right=of w] (w') {\type{gauss_wrapper.o}};
\node[draw,below=of w'] (g') {\type{gauss.o}};

\node[draw,right=of w'] (f) {\type{gauss}};

\draw[->] (w) to node[above] {\type{cc -c}} (w');
\draw[->] (g) to node[above] {\type{nasm}} (g');

\draw[->] (w') to coordinate[midway] (m) node[above] {\type{cc}} (f);
\draw[->] (g') -| (m);

\stoptikzpicture
}
\stopplacefigure

The last step is executing our program from the command line, an example
output is shown here:
\startnocode
$ ./gauss
Not enough arguments!
$ ./gauss 2
gauss(2) = 140721287320512[WRONG]
\stopnocode
We can see that despite our function returning \quote{nothing} the wrapper
prints a result, which might be suprising at first.
\stopsubject


\startsubject[title={x86 ISA}]
In order to extend our program, we need to understand some basics
about x86---although most of it applies to many other architectures, just with
different names and slighly changed semantics.

The most basic concept when programming Assembly are \quote{registers}.
Our x86 CPU has quite a number of them and what they do is store \unit{64 bit}
numbers.\footnote{%
  There are differently sized registers as well, but we will have a look at
  them a different time.
}
For different, mostly historic, reasons they are somewhat crudely named on x86,
but you can look them up any time.
%
\startplacetable[reference=tbl:regs,title={x86 (AMD64) \unit{64 bit} Registers}]
\starttabulate[|l|l|l|l|l|]
\FL
\NC rax \NC rdi  \NC r8  \NC r12 \NC (rip)    \NC\NR
\NC rbx \NC rsi  \NC r9  \NC r13 \NC (rflags) \NC\NR
\NC rcx \NC rsp  \NC r10 \NC r14 \NC          \NC\NR
\NC rdx \NC rbp  \NC r11 \NC r15 \NC          \NC\NR
\LL
\stoptabulate
\stopplacetable
%
(Almost) everything we do simply manipulates these registers contents:
We can add two values in two registers, copy values from one to another, overwrite
values with fixed constants, etc\@.

For now we will only look at arithmetics, as that's all we need for the
Gauss sum.
Let's try to modify our program like this:
\startasmcode
global gauss

gauss:  mov rax, 30     ; rax := 30
        mov rcx, 12     ; rcx := 12
        add rax, rcx    ; rax := rax + rcx
        ret
\stopasmcode
This will instruct the computer to add $30+12$ and store the result in \type{rax}
and then return the execution from our function back to our wrapper.

We now need to run the assembler again, to produce a new \type{gauss.o} and
then run the compiler to produce the new \type{gauss} executable.
We can test:
\startnocode
$ ./gauss 2
gauss(2) = 42[WRONG]
\stopnocode

While this is unsurpisingly still wrong, 
e can now understand better what the wrapper was
printing in the first version of our program:  The value of the register
\type{rax} which just happened to be $140721287320512$ in my case.  If we
can control the value of \type{rax}, we can control what our wrapper will print!
\stopsubject


\startsubject[title={Calling Convention}]

There's no \quote{real} reason (as far as we are concerned) why our wrapper
assumes the result to be in the register \type{rax}, it's a convention.
This convention was developed by different operating systems vendors for the
x86 architecture and as part of the clumsily named System~V AMD64 ABI\@.
There are other conventions that could be used in place of this one, but
we will adhere to this \emph{Calling Convention} everywhere, unless specified
differently.
This is also the place where we need to look if we want to not only output the
same result all the time, but make it depend on the input.

The exercise specifies that our wrapper will take the input the user gives the program
on the console, and in turn pass it to our function as its first (and only)
argument.
A quick look in the Calling Convention tells us that we can expect our input to
be in the register \type{rdi}.
But the contents of all other registers are unknown to us!
With this knowledge, we can modify our program again, and instead of adding
$12$ to $30$, we add whatever the user gave our program:
\startasmcode
global gauss

; 1st argument: rdi
; return value: rax
gauss:  mov rax, 30     ; rax := 30
        add rax, rdi    ; rax := rax + rdi
        ret
\stopasmcode
Try and see for yourself!

%
\startplacetable[reference=tbl:cc-regs,title={System V AMD64 CC: Parameters}]
\starttabulate[|l|l|l|l|l|l|l|l|]
\FL
\NC 1st \NC 2nd \NC 3rd \NC 4th \NC 5th \NC 6th \VL 7th,8th,\dots \VL return \NC\NR
\HL
\NC rdi \NC rsi \NC rdx \NC rcx \NC r8  \NC r9  \VL stack         \VL rax    \NC\NR
\LL
\stoptabulate
\stopplacetable
%

This has a simple effect: Not all registers are created equal.  If we'd replace
\type{rdi} with \type{rsi} in the last example, the output would (likely) be
vastly different.\footnote{%
  We could be \quote{lucky} and happen to be in the situation where for some
  reason \type{rsi} has the same value as \type{rdi}.
}
The Calling Convention also has a few other restrictions for us:  We aren't
simply allowed to use every register as freely as we might want.
Specifically, the convention defines so-called \quote{volatile} registers:
These are the ones we are free to change within our function as \emph{our
wrapper can't expect those to be the same after our code ran}.
However, our wrapper expects the following, non-volatile, registers to hold the
same value after your function ran as was before: \type{rbx}, \type{rsp},
\type{rbp}, and \type{r12}--\type{r15}.
Luckily we didn't use any of these yet, so we just keep it that way, for now.

%
\startplacetable[reference=tbl:cc-vol,title={System V AMD64 CC: Volatile/Non-Volatile}]
\starttabulate[|l|l|l|l|l|]
\FL
\NC rax      \NC rdi       \NC r8  \CM[red] r12 \CM[red] (rip)    \NC\NR
\CM[red] rbx \NC rsi       \NC r9  \CM[red] r13 \CM[red] (rflags) \NC\NR
\NC rcx      \CM[red] rsp  \NC r10 \CM[red] r14 \NC               \NC\NR
\NC rdx      \CM[red] rbp  \NC r11 \CM[red] r15 \NC               \NC\NR
\LL
\stoptabulate
\stopplacetable
%

Furthermore, you can't directly access the registers \type{rip} (instruction
pointer) and \type{rflags} (flags register), this is \emph{technically}
impossible due to the restrictions of x86.
Similarly, other registers serve another special purpose as well, which we will
come back to as needed.
\stopsubject


\startsubject[title={Short x86 ISA Reference}]

For the exercise at hand you only need a limited set of the thousands of
instructions available.
While you've already learned of the instructions \type{mov}, \type{add} and
\type{ret}, some are a bit more more complicated in their semantics.
This is the case for the \type{mul} and \type{div} instructions:  Both only
accept one operand, despite a multiplication with just one multiplicand being
quite\dots\ uninteresting.

These instructions---for reasons that do not matter here---have pre-specified
arguments.
That is, if you want to calculate $6\cdot 7$, you must do this like this:
\startasmcode
mov rax, 6
mov rcx, 7
mul rcx     ; rdx:rax := rax * rcx
\stopasmcode
The \type{mul} instruction multiplies its operand register with the implied
register \type{rax}.
Furthermore, as multiplications of two \unit{64 bit} numbers can easily get
quite large, the result is stored as \unit{128 bit} number.
We do not have such big registers, so we need to store the upper half and lower
half separately, in the registers \type{rdx} and \type{rax} respectively.
This means, if the result is small enough to fit into \type{rax} alone,
\type{rdx} has the value $0$ as it only consists of leading zeroes.\footnote{%
  This simply boils down to $00031415$ and $31415$ being the same number.
  The way our processor works simply forces us to use \digits{128} binary digits.
}

The division conversely divides the \unit{128 bit} number consisting of the
concatenation of \type{rdx:rax} by its operand.
Thus, to divide $126 \div 3$, you need to store the dividend in it's \unit{128 bit}
represenation, with the upper half filling \type{rdx}.
As $126 < 2^{64}$, the upper half will be simply zero.
\startasmcode
mov rax, 126
mov rdx, 0      ; 64 leading zeroes
mov rcx, 3
div rcx         ; rax := rdx:rax ÷ rcx
\stopasmcode
In contrast to maths however, if we forget to clear \type{rdx} to zero, it's
(likely non-zero) contents will be seen as the upper half of our dividend,
resulting in quite a different result.

\stopsubject

\stopproduct
